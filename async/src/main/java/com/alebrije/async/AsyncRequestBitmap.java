package com.alebrije.async;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class AsyncRequestBitmap extends AsyncTask<Void, Integer, Bitmap> {
    private WeakReference<Context> weakContext;
    private WeakReference<ContentLoadingProgressBar> weakProgressBar;
    private Map<String, List<String>> headerFields;
    private OnResponseListener listener;
    private String logTag = "AsyncRequest";
    private String data;
    private String method;
    private String url;
    private String[][] headers;
    private String error;
    private int responseCode = 417;
    private int connectTimeout = 0;
    private int readTimeout = 0;

    /**
     *
     * @param method the HTTP request method
     * @param url the String to parse as a URL
     * @param context the Context the request is running in, through which it can access the connectivity
     * @param listener the callback that will run
     */
    public AsyncRequestBitmap(@NonNull String method, @NonNull String url, @Nullable Context context, @Nullable OnResponseListener listener) {
        this.listener = listener;
        this.weakContext = new WeakReference<>(context);
        this.method = method;
        this.url = url;
        this.data = null;
        this.error = null;
        this.headers = null;
    }

    @Override
    protected void onPreExecute() {
        ContentLoadingProgressBar progressBar = weakProgressBar == null ? null : weakProgressBar.get();
        if (progressBar != null) {
            progressBar.setIndeterminate(true);
            /*
            //TODO: Implement progress
            progressBar.setProgress(0);
            progressBar.setMax(100);
            */
            progressBar.show();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        ContentLoadingProgressBar progressBar = weakProgressBar == null ? null : weakProgressBar.get();
        if (progressBar != null) {
            progressBar.setProgress(values[0]);
        }
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        if (url.isEmpty()) {
            error = "URL is empty";
            return null;
        } else if (!isOnline()) {
            error = "No connection";
            return null;
        }

        //Log.d(logTag, method + ": " + url);
        Bitmap bitmap = null;
        try {
            // Prepare request
            HttpURLConnection urlConnection = (HttpURLConnection) new URL(url).openConnection();
            urlConnection.setConnectTimeout(connectTimeout);
            urlConnection.setReadTimeout(readTimeout);
            urlConnection.setRequestMethod(method);

            // Add HEADERS if requested
            if (headers != null) {
                //Log.d(logTag, "HEADERS: " + headers.toString());
                for (String[] header : headers) {
                    if (header == null || header.length < 2) {
                        Log.w(logTag, "WARNING: Unable to add header " + header);
                    } else {
                        urlConnection.setRequestProperty(header[0], header[1]);
                    }
                }
            }

            // Add POST data if requested
            if (data != null) {
                //Log.d(logTag, "DATA: " + data);
                urlConnection.setDoOutput(true);
                DataOutputStream outputStream = new DataOutputStream(urlConnection.getOutputStream());
                outputStream.writeBytes(data);
                outputStream.flush();
                outputStream.close();
            }

            responseCode = urlConnection.getResponseCode();
            if (responseCode >= 400) {
                error = urlConnection.getResponseMessage();
            }
            headerFields = urlConnection.getHeaderFields();

            try {
                // Read response
                InputStream inputStream = urlConnection.getInputStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap response) {
        ContentLoadingProgressBar progressBar = weakProgressBar == null ? null : weakProgressBar.get();
        if (progressBar != null) {
            progressBar.hide();
        }
        if (response == null) {
            if (error == null) {
                error = "Response is null";
            }
            Log.e(logTag, "ERROR: " + error);
        }
        if (listener != null) {
            listener.onResponse(responseCode, headerFields, response, error);
        }
    }

    /**
     * Sets a specified timeout value, in milliseconds, to be used when opening a communications link to the resource referenced.
     *
     * @param timeout an int that specifies the connect timeout value in milliseconds
     * @return this Async Request object to allow for chaining of calls to set methods.
     */
    public AsyncRequestBitmap setConnectTimeout(int timeout) {
        this.connectTimeout = timeout;
        return this;
    }

    /**
     *
     * @param progressBar the content loading progress bar to update with the request progress
     * @return this Async Request object to allow for chaining of calls to set methods.
     */
    public AsyncRequestBitmap setContentLoadingProgressBar(ContentLoadingProgressBar progressBar) {
        this.weakProgressBar = new WeakReference<>(progressBar);
        return this;
    }

    /**
     *
     * @param data a string with the data to send in the request
     * @return this Async Request object to allow for chaining of calls to set methods.
     */
    public AsyncRequestBitmap setData(String data) {
        this.data = data;
        return this;
    }

    /**
     *
     * @param headers a string pair array with the headers to send in the request
     * @return this Async Request object to allow for chaining of calls to set methods.
     */
    public AsyncRequestBitmap setHeaders(String[][] headers) {
        this.headers = headers;
        return this;
    }

    /**
     * Sets the read timeout to a specified timeout, in milliseconds.
     *
     * @param timeout an int that specifies the timeout value to be used in milliseconds
     * @return this Async Request object to allow for chaining of calls to set methods.
     */
    public AsyncRequestBitmap setReadTimeout(int timeout) {
        this.readTimeout = timeout;
        return this;
    }

    private boolean isOnline() {
        Context context = weakContext.get();
        if (context == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager != null && connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    public interface OnResponseListener {
        /**
         *
         * @param responseCode the response code
         * @param headerFields the response header fields
         * @param response the response bitmap
         * @param error the error description, in case of an error
         */
        void onResponse(int responseCode, @Nullable Map<String, List<String>> headerFields, @Nullable Bitmap response, @Nullable String error);
    }
}

package com.alebrije.async;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.util.LruCache;

import java.util.List;
import java.util.Map;

public class ImageCache {
    private static int _cacheSize = (int) (Runtime.getRuntime().maxMemory() / 1024) / 4;
    private static LruCache<String, Bitmap> _imageCache = new LruCache<String, Bitmap>(_cacheSize) {
        @Override
        protected int sizeOf(String key, Bitmap value) {
            return value.getByteCount() / 1024;
        }
    };

    /**
     *
     * @return the current cache size
     */
    public static int getCacheSize() {
        return _cacheSize;
    }

    /**
     * Clears current image cache.
     *
     * @deprecated Not required anymore. Memory is now managed internally.
     */
    @Deprecated
    public static void clear() {
        //imageCache.clear();
    }

    /**
     *
     * @param url the String to parse as a URL
     * @param context the Context the request is running in, through which it can access the connectivity
     */
    public static void getImage(@NonNull String url, @Nullable Context context) {
        getImage(url, context, null, null);
    }

    /**
     *
     * @param url the String to parse as a URL
     * @param context the Context the request is running in, through which it can access the connectivity
     * @param progressBar the content loading progress bar to update with the request progress
     */
    public static void getImage(@NonNull String url, @Nullable Context context, @Nullable ContentLoadingProgressBar progressBar) {
        getImage(url, context, progressBar, null);
    }

    /**
     *
     * @param url the String to parse as a URL
     * @param context the Context the request is running in, through which it can access the connectivity
     * @param listener the callback that will run
     */
    public static void getImage(@NonNull final String url, @Nullable Context context, @Nullable OnResponseListener listener) {
        getImage(url, context, null, listener);
    }

    /**
     *
     * @param url the String to parse as a URL
     * @param context the Context the request is running in, through which it can access the connectivity
     * @param progressBar the content loading progress bar to update with the request progress
     * @param listener the callback that will run
     */
    public static void getImage(@NonNull final String url, @Nullable Context context, @Nullable ContentLoadingProgressBar progressBar, @Nullable final OnResponseListener listener) {
        if (url.isEmpty()) {
            if (listener != null) {
                listener.onResponse(null, "URL is empty");
            }
        } else {
            Bitmap bitmap = _imageCache.get(url);
            if (bitmap == null) {
                new AsyncRequestBitmap("GET", url, context, new AsyncRequestBitmap.OnResponseListener() {
                    @Override
                    public void onResponse(int responseCode, @Nullable Map<String, List<String>> responseHeaders, @Nullable Bitmap response, @Nullable String error) {
                        if (response != null) {
                            _imageCache.put(url, response);
                        }
                        if (listener != null) {
                            listener.onResponse(response, error);
                        }
                    }
                }).setContentLoadingProgressBar(progressBar).execute();
            } else {
                if (listener != null) {
                    listener.onResponse(_imageCache.get(url), null);
                }
            }
        }
    }

    public interface OnResponseListener {
        /**
         *
         * @param response the response bitmap
         * @param error the error description, in case of an error
         */
        void onResponse(@Nullable Bitmap response, @Nullable String error);
    }
}

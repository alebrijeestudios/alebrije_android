package com.alebrije.async;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class AsyncRequestString extends AsyncTask<Void, Integer, String> {
    private WeakReference<Context> weakContext;
    private WeakReference<ContentLoadingProgressBar> weakProgressBar;
    private Map<String, List<String>> headerFields;
    private OnResponseListener listener;
    private String logTag = "AsyncRequest";
    private Object data;
    private String method;
    private String url;
    private String[][] headers;
    private String error;
    private Exception exception;
    private int responseCode = 417;
    private int connectTimeout = 0;
    private int readTimeout = 0;

    /**
     *
     * @param method the HTTP request method
     * @param url the String to parse as a URL
     * @param context the Context the request is running in, through which it can access the connectivity
     * @param listener the callback that will run
     */
    public AsyncRequestString(@NonNull String method, @NonNull String url, @Nullable Context context, @Nullable OnResponseListener listener) {
        this.listener = listener;
        this.weakContext = new WeakReference<>(context);
        this.method = method;
        this.url = url;
        this.data = null;
        this.error = null;
        this.headers = null;
        this.exception = null;
    }

    @Override
    protected void onPreExecute() {
        ContentLoadingProgressBar progressBar = weakProgressBar == null ? null : weakProgressBar.get();
        if (progressBar != null) {
            progressBar.setIndeterminate(true);
            /*
            //TODO: Implement progress
            progressBar.setProgress(0);
            progressBar.setMax(100);
            */
            progressBar.show();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        ContentLoadingProgressBar progressBar = weakProgressBar == null ? null : weakProgressBar.get();
        if (progressBar != null) {
            progressBar.setProgress(values[0]);
        }
    }

    @Override
    protected String doInBackground(Void... params) {
        if (url.isEmpty()) {
            error = "URL is empty";
            return null;
        } else if (!isOnline()) {
            error = "No connection";
            return null;
        }

        //Log.d(logTag, method + ": " + url);
        String response = null;
        try {
            // Prepare request
            HttpURLConnection urlConnection = (HttpURLConnection) new URL(url).openConnection();
            urlConnection.setConnectTimeout(connectTimeout);
            urlConnection.setReadTimeout(readTimeout);
            urlConnection.setRequestMethod(method);

            // Add HEADERS if requested
            if (headers != null) {
                //Log.d(logTag, "HEADERS: " + headers.toString());
                for (String[] header : headers) {
                    if (header == null || header.length < 2) {
                        Log.w(logTag, "WARNING: Unable to add header " + header);
                    } else {
                        urlConnection.setRequestProperty(header[0], header[1]);
                    }
                }
            }

            // Add POST data if requested
            if (data != null) {
                //Log.d(logTag, "DATA: " + data);
                urlConnection.setDoOutput(true);
                if (data instanceof Object[][]) {
                    String boundary = "----[" + System.currentTimeMillis() + "]";
                    urlConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                    DataOutputStream outputStream = new DataOutputStream(urlConnection.getOutputStream());
                    for (Object[] entry: (Object[][])data) {
                        if (entry.length < 2) {
                            Log.e(logTag, "Cannot process entry: Not enough parameters");
                            continue;
                        }
                        outputStream.writeBytes("--" + boundary + "\r\n");
                        if (entry[1] instanceof File) {
                            File file = (File) entry[1];
                            FileInputStream inputStream = new FileInputStream(file);
                            byte[] bytes = new byte[(int) file.length()];
                            try {
                                int read = 0;
                                while (read != -1) {
                                    read = inputStream.read(bytes);
                                }
                                inputStream.close();

                                outputStream.writeBytes("Content-Disposition: form-data; name=\"" + entry[0] + "\"; filename=\"" + file.getName() + "\"\r\n");
                                //outputStream.writeBytes("Content-Type: \"" + file.getType() + "\"\r\n");
                                outputStream.writeBytes("\r\n");
                                outputStream.write(bytes);
                                outputStream.writeBytes("\r\n");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (entry[1] instanceof byte[]) {
                            if (entry.length > 2) {
                                outputStream.writeBytes("Content-Disposition: form-data; name=\"" + entry[0] + "\"; filename=\"" + entry[2] + "\"\r\n");
                            } else {
                                outputStream.writeBytes("Content-Disposition: form-data; name=\"" + entry[0] + "\"; filename=\"file.dat\"\r\n");
                            }
                            if (entry.length > 3) {
                                outputStream.writeBytes("Content-Type: \"" + entry[3] + "\"\r\n");
                            }
                            outputStream.writeBytes("\r\n");
                            outputStream.write((byte[]) entry[1]);
                            outputStream.writeBytes("\r\n");
                        } else {
                            outputStream.writeBytes("Content-Disposition: form-data; name=\"" + entry[0] + "\"\r\n");
                            outputStream.writeBytes("\r\n");
                            outputStream.write(entry[1].toString().getBytes("UTF-8"));
                            outputStream.writeBytes("\r\n");
                        }
                    }
                    outputStream.writeBytes("--" + boundary + "--\r\n");
                    outputStream.close();
                } else {
                    DataOutputStream outputStream = new DataOutputStream(urlConnection.getOutputStream());
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    bufferedWriter.write(data.toString());
                    bufferedWriter.close();
                    outputStream.close();
                }
            }

            responseCode = urlConnection.getResponseCode();
            if (responseCode >= 400) {
                error = urlConnection.getResponseMessage();
            }
            headerFields = urlConnection.getHeaderFields();

            InputStream inputStream = null;
            if (responseCode >= 400) {
                try {
                    inputStream = urlConnection.getErrorStream();
                } catch (Exception e) {
                    exception = e;
                    e.printStackTrace();
                }
            }
            if (inputStream == null) {
                try {

                    inputStream = urlConnection.getInputStream();
                } catch (Exception e) {
                    exception = e;
                    e.printStackTrace();
                }
            }
            try {
                if (inputStream == null) {
                    Log.w(logTag, "WARNING: Input Stream is empty");
                } else {
                    // Convert response to string
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;

                    StringBuilder stringBuilder = new StringBuilder("");
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    response = stringBuilder.toString();

                    inputStream.close();
                }
            } catch (Exception e) {
                exception = e;
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            exception = e;
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(String response) {
        //Log.d(logTag, "Response: " + response);
        ContentLoadingProgressBar progressBar = weakProgressBar == null ? null : weakProgressBar.get();
        if (progressBar != null) {
            progressBar.hide();
        }
        if (response == null) {
            if (error == null) {
                error = "Response is null";
            }
            Log.e(logTag, "ERROR: " + error);
        }
        if (listener != null) {
            listener.onResponse(responseCode, headerFields, response, error, exception);
        }
    }

    /**
     * Sets a specified timeout value, in milliseconds, to be used when opening a communications link to the resource referenced.
     *
     * @param timeout an int that specifies the connect timeout value in milliseconds
     * @return this Async Request object to allow for chaining of calls to set methods.
     */
    public AsyncRequestString setConnectTimeout(int timeout) {
        this.connectTimeout = timeout;
        return this;
    }

    /**
     *
     * @param progressBar the content loading progress bar to update with the request progress
     * @return this Async Request object to allow for chaining of calls to set methods.
     */
    public AsyncRequestString setContentLoadingProgressBar(ContentLoadingProgressBar progressBar) {
        this.weakProgressBar = new WeakReference<>(progressBar);
        return this;
    }

    /**
     *
     * @param data a string or string-object pair array with the data to send in the request
     * @return this Async Request object to allow for chaining of calls to set methods.
     */
    public AsyncRequestString setData(Object data) {
        this.data = data;
        return this;
    }

    /**
     *
     * @param headers a string pair array with the headers to send in the request
     * @return this Async Request object to allow for chaining of calls to set methods.
     */
    public AsyncRequestString setHeaders(String[][] headers) {
        this.headers = headers;
        return this;
    }

    /**
     * Sets the read timeout to a specified timeout, in milliseconds.
     *
     * @param timeout an int that specifies the timeout value to be used in milliseconds
     * @return this Async Request object to allow for chaining of calls to set methods.
     */
    public AsyncRequestString setReadTimeout(int timeout) {
        this.readTimeout = timeout;
        return this;
    }

    private boolean isOnline() {
        Context context = weakContext.get();
        if (context == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager != null && connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    public interface OnResponseListener {
        /**
         *
         * @param responseCode the response code
         * @param headerFields the response header fields
         * @param response the response content
         * @param error the error description, in case of an error
         */
        void onResponse(int responseCode, @Nullable Map<String, List<String>> headerFields, @Nullable String response, @Nullable String error, @Nullable Exception exception);
    }
}

package com.alebrije.android

/**
 * Created by alebrije4 on 12/16/16.
 *
 */

object ApiUrl {
    val ACCESS_TOKEN = "a6e8510141cae3c489326dfe2369989332d8ed35"
    val SERVER = "https://alebrije.co"
    val NEWS = SERVER + "/api/news.json"
    val PASSWORD_RECOVERY = SERVER + "/api/users/password-recovery.json"
    val PROJECTS = SERVER + "/api/projects.json"
    val PROJECT_TYPES = SERVER + "/api/types.json"
    val USERS = SERVER + "/api/users.json"
}

package com.alebrije.android.models

import android.content.Context

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import org.json.JSONArray
import org.json.JSONObject

import java.io.Serializable

/**
 * Created by alebrije-air1 on 9/29/17.
 *
 */

class User(jsonObject: JSONObject) : Serializable {
    var id = jsonObject.optInt("id")
    var name: String = jsonObject.optString("name", "")
    var email: String = jsonObject.optString("email", "")

    fun saveLocal(context: Context?, key: String? = null) {
        val currentKey = key ?: PREFERENCE_KEY
        val jsonString = Gson().toJson(this)
        context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
                ?.edit()
                ?.putString(currentKey, jsonString)
                ?.apply()
    }

    companion object {
        private val PREFERENCE_KEY = "user"

        fun arrayFromJSONArray(jsonArray: JSONArray): ArrayList<User> {
            val list = ArrayList<User>()
            (0 until jsonArray.length()).mapTo(list) {
                User(jsonArray.optJSONObject(it))
            }
            return list
        }

        fun getLocal(context: Context?, key: String? = null): User? {
            val currentKey = key ?: PREFERENCE_KEY
            val sharedPreferences = context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
            if (sharedPreferences != null && sharedPreferences.contains(currentKey)) {
                val jsonString = sharedPreferences.getString(currentKey, null)
                if (jsonString != null) {
                    return Gson().fromJson<User>(jsonString, object : TypeToken<User>() {

                    }.type)
                }
            }
            return null
        }

        fun getLocalArray(context: Context?, key: String? = null): ArrayList<User>? {
            val currentKey = key ?: PREFERENCE_KEY + "Array"
            val sharedPreferences = context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
            if (sharedPreferences != null && sharedPreferences.contains(currentKey + "Array")) {
                val jsonString = sharedPreferences.getString(currentKey + "Array", null)
                if (jsonString != null) {
                    return Gson().fromJson<ArrayList<User>>(jsonString, object : TypeToken<ArrayList<User>>() {

                    }.type)
                }
            }
            return null
        }

        fun hasLocal(context: Context?, key: String? = null): Boolean {
            val currentKey = key ?: PREFERENCE_KEY
            return context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)?.contains(currentKey) ?: false
        }

        fun hasLocalArray(context: Context?, key: String? = null): Boolean {
            val currentKey = key ?: PREFERENCE_KEY + "Array"
            return context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)?.contains(currentKey) ?: false
        }

        fun removeLocal(context: Context?, key: String? = null) {
            val currentKey = key ?: PREFERENCE_KEY
            context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
                    ?.edit()
                    ?.remove(currentKey)
                    ?.apply()
        }

        fun removeLocalArray(context: Context?, key: String? = null) {
            val currentKey = key ?: PREFERENCE_KEY + "Array"
            context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
                    ?.edit()
                    ?.remove(currentKey)
                    ?.apply()
        }

        fun saveLocalArray(array: List<User>, context: Context?, key: String? = null) {
            val currentKey = key ?: PREFERENCE_KEY + "Array"
            val jsonString = Gson().toJson(array)
            context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
                    ?.edit()
                    ?.putString(currentKey, jsonString)
                    ?.apply()
        }
    }
}

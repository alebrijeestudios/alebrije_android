package com.alebrije.android.models

import android.content.Context

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import org.json.JSONArray
import org.json.JSONObject

import java.io.Serializable
import java.util.Locale

/**
 * Created by alebrije4 on 12/16/16.
 *
 */

class Project(jsonObject: JSONObject) : Serializable {
    var id = jsonObject.optInt("id")
    var typeId = jsonObject.optInt("type_id")
    var name: String = jsonObject.optString("name", "")
    var client: String = jsonObject.optString("client_name", "")
    var imageSrc: String = jsonObject.optString("image_src", "")
    var localizedDescription: String = when (Locale.getDefault().language) {
        "es" -> jsonObject.optString("description_es", "")
        else -> jsonObject.optString("description_en", "")
    }

    companion object {
        private val PREFERENCE_KEY = "project"

        fun arrayFromJSONArray(jsonArray: JSONArray): ArrayList<Project> {
            val list = ArrayList<Project>()
            (0 until jsonArray.length()).mapTo(list) {
                Project(jsonArray.optJSONObject(it))
            }
            return list
        }

        fun getLocalArray(context: Context?, key: String? = null): ArrayList<Project>? {
            val currentKey = key ?: PREFERENCE_KEY + "Array"
            val sharedPreferences = context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
            if (sharedPreferences != null && sharedPreferences.contains(currentKey)) {
                val jsonString = sharedPreferences.getString(currentKey, null)
                if (jsonString != null) {
                    return Gson().fromJson<ArrayList<Project>>(jsonString, object : TypeToken<ArrayList<Project>>() {

                    }.type)
                }
            }
            return null
        }

        fun hasLocalArray(context: Context?, key: String? = null): Boolean {
            val currentKey = key ?: PREFERENCE_KEY + "Array"
            return context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)?.contains(currentKey) ?: false
        }

        fun removeLocalArray(context: Context?, key: String? = null) {
            val currentKey = key ?: PREFERENCE_KEY + "Array"
            context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
                    ?.edit()
                    ?.remove(currentKey)
                    ?.apply()
        }

        fun saveLocalArray(array: List<Project>, context: Context?, key: String? = null) {
            val currentKey = key ?: PREFERENCE_KEY + "Array"
            val jsonString = Gson().toJson(array)
            context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
                    ?.edit()
                    ?.putString(currentKey, jsonString)
                    ?.apply()
        }
    }
}

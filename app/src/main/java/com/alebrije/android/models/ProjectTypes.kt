package com.alebrije.android.models

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable
import java.util.*

/**
 * Created by alebrije-air1 on 11/30/17.
 *
 */


class ProjectTypes(jsonObject: JSONObject) : Serializable {
    var id = jsonObject.optInt("id")
    var localizedName: String = when (Locale.getDefault().language) {
        "es" -> jsonObject.optString("name_es", "")
        else -> jsonObject.optString("name_en", "")
    }

    companion object {
        private val PREFERENCE_KEY = "projectType"

        fun arrayFromJSONArray(jsonArray: JSONArray): ArrayList<ProjectTypes> {
            val list = ArrayList<ProjectTypes>()
            (0 until jsonArray.length()).mapTo(list) {
                ProjectTypes(jsonArray.optJSONObject(it))
            }
            return list
        }

        fun getLocalArray(context: Context?, key: String? = null): ArrayList<ProjectTypes>? {
            val currentKey = key ?: PREFERENCE_KEY + "Array"
            val sharedPreferences = context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
            if (sharedPreferences != null && sharedPreferences.contains(currentKey)) {
                val jsonString = sharedPreferences.getString(currentKey, null)
                if (jsonString != null) {
                    return Gson().fromJson<ArrayList<ProjectTypes>>(jsonString, object : TypeToken<ArrayList<ProjectTypes>>() {

                    }.type)
                }
            }
            return null
        }

        fun hasLocalArray(context: Context?, key: String? = null): Boolean {
            val currentKey = key ?: PREFERENCE_KEY + "Array"
            return context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)?.contains(currentKey) ?: false
        }

        fun removeLocalArray(context: Context?, key: String? = null) {
            val currentKey = key ?: PREFERENCE_KEY + "Array"
            context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
                    ?.edit()
                    ?.remove(currentKey)
                    ?.apply()
        }

        fun saveLocalArray(array: List<ProjectTypes>, context: Context?, key: String? = null) {
            val currentKey = key ?: PREFERENCE_KEY + "Array"
            val jsonString = Gson().toJson(array)
            context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
                    ?.edit()
                    ?.putString(currentKey, jsonString)
                    ?.apply()
        }
    }
}

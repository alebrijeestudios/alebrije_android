package com.alebrije.android.models

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONArray
import org.json.JSONObject

import java.io.Serializable
import java.util.Locale

/**
 * Created by alebrije4 on 12/16/16.
 *
 */

class News(jsonObject: JSONObject) : Serializable {
    var id = jsonObject.optInt("id")
    var localizedDescription: String = when (Locale.getDefault().language) {
        "es" -> jsonObject.optString("description_es", "")
        else -> jsonObject.optString("description_en", "")
    }

    companion object {
        private val PREFERENCE_KEY = "news"

        fun arrayFromJSONArray(jsonArray: JSONArray): ArrayList<News> {
            val list = ArrayList<News>()
            (0 until jsonArray.length()).mapTo(list) {
                News(jsonArray.optJSONObject(it))
            }
            return list
        }

        fun getLocalArray(context: Context?, key: String? = null): ArrayList<News>? {
            val currentKey = key ?: PREFERENCE_KEY + "Array"
            val sharedPreferences = context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
            if (sharedPreferences != null && sharedPreferences.contains(currentKey)) {
                val jsonString = sharedPreferences.getString(currentKey, null)
                if (jsonString != null) {
                    return Gson().fromJson<ArrayList<News>>(jsonString, object : TypeToken<ArrayList<News>>() {

                    }.type)
                }
            }
            return null
        }

        fun hasLocalArray(context: Context?, key: String? = null): Boolean {
            val currentKey = key ?: PREFERENCE_KEY + "Array"
            return context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)?.contains(currentKey) ?: false
        }

        fun removeLocalArray(context: Context?, key: String? = null) {
            val currentKey = key ?: PREFERENCE_KEY + "Array"
            context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
                    ?.edit()
                    ?.remove(currentKey)
                    ?.apply()
        }

        fun saveLocalArray(array: List<News>, context: Context?, key: String? = null) {
            val currentKey = key ?: PREFERENCE_KEY + "Array"
            val jsonString = Gson().toJson(array)
            context?.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
                    ?.edit()
                    ?.putString(currentKey, jsonString)
                    ?.apply()
        }
    }
}

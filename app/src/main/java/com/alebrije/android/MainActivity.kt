package com.alebrije.android

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.util.Log
import android.view.View
import android.view.animation.TranslateAnimation
import android.widget.Toast
import com.alebrije.android.models.User
import com.alebrije.async.ImageCache
import com.alebrije.core.SimpleFragmentManager

import com.alebrije.core.SimpleFragmentManagerActivity

class MainActivity : SimpleFragmentManagerActivity() {
    private var appName = ""
    private var bottomNavigationContainer: View? = null
    private var bottomNavigationHidden = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        SimpleFragmentManager.setContainer(R.id.container)

        appName = getString(R.string.app_name)

        // Bottom navigation menu
        bottomNavigationContainer = findViewById(R.id.bottomNavigationContainer)

        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            val currentFragment = supportFragmentManager.findFragmentById(R.id.container)
            when (item.itemId) {
                R.id.action_news -> if (currentFragment is NewsListFragment) {
                    currentFragment.scrollToTop()
                } else {
                    SimpleFragmentManager.setRootFragment(NewsListFragment())
                }
                R.id.action_projects -> if (currentFragment is ProjectListFragment) {
                    currentFragment.scrollToTop()
                } else {
                    SimpleFragmentManager.setRootFragment(ProjectListFragment())
                }
                R.id.action_user -> if (currentFragment is UserFragment) {
                    currentFragment.scrollToTop()
                } else if (currentFragment is LoginFragment) {
                    currentFragment.scrollToTop()
                } else if (currentFragment is PasswordRecoveryFragment) {
                    currentFragment.scrollToTop()
                } else {
                    if (User.hasLocal(this)) {
                        SimpleFragmentManager.setRootFragment(UserFragment())
                    } else {
                        SimpleFragmentManager.setRootFragment(LoginFragment())
                    }
                }
            }
            true
        }

        findViewById<View>(R.id.bottomNavigationBarView).visibility = if (hasNavBar()) View.VISIBLE else View.GONE

        if ("projects" == intent.getStringExtra("fragment")) {
            bottomNavigationView.selectedItemId = R.id.action_projects
        } else {
            SimpleFragmentManager.setFragment(NewsListFragment())
        }

        // Debug version
        if (BuildConfig.DEBUG) {
            Toast.makeText(this, appName + " v" + BuildConfig.VERSION_NAME + " (" + BuildConfig.VERSION_CODE + ")", Toast.LENGTH_SHORT).show()
        }
        Log.d(appName, "Cache size: " + ImageCache.getCacheSize())
    }

    fun hideBottomNavigation(hide: Boolean) {
        if (bottomNavigationHidden == hide) {
            return
        }
        bottomNavigationHidden = hide
        if (bottomNavigationHidden) {
            val animation = TranslateAnimation(0f, 0f, 0f, bottomNavigationContainer?.height?.toFloat() ?: 0f)
            animation.duration = 200
            bottomNavigationContainer?.startAnimation(animation)
            bottomNavigationContainer?.visibility = View.GONE
        } else {
            bottomNavigationContainer?.visibility = View.VISIBLE
            val animation = TranslateAnimation(0f, 0f, bottomNavigationContainer?.height?.toFloat() ?: 0f, 0f)
            animation.duration = 200
            bottomNavigationContainer?.startAnimation(animation)
        }
    }

    private fun hasNavBar (): Boolean {
        val id = resources.getIdentifier("config_showNavigationBar", "bool", "android")
        return id > 0 && resources.getBoolean(id)
    }
}

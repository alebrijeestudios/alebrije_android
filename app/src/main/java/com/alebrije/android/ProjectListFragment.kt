package com.alebrije.android


import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.Fragment
import android.support.v4.widget.ContentLoadingProgressBar
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast

import com.alebrije.android.models.Project
import com.alebrije.android.models.ProjectTypes
import com.alebrije.async.AsyncRequestString
import com.alebrije.async.ImageCache
import com.alebrije.core.SimpleRecyclerAdapter
import org.json.JSONObject

import java.util.ArrayList


/**
 * A simple [Fragment] subclass.
 */
class ProjectListFragment : Fragment() {
    private val limitPerPage = 30

    private var adapter: SimpleRecyclerAdapter? = null
    private var appName = ""
    private var progressBar: ContentLoadingProgressBar? = null
    private var spinner: Spinner? = null
    private var spinnerAdapter: ArrayAdapter<String>? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null

    private var currentPage = 0
    private var projectTypeId = 0
    private var totalProjects = 0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        appName = getString(R.string.app_name)

        adapter = object : SimpleRecyclerAdapter(R.layout.fragment_project_grid_item) {
            override fun onListItem(listItem: Any?, itemView: View, position: Int) {
                val projectItemImageView = itemView.findViewById<ImageView>(R.id.projectItem_imageView)
                val titleProjectItemTextView = itemView.findViewById<TextView>(R.id.projectItem_titleTextView)
                val subtitleProjectItemTextView = itemView.findViewById<TextView>(R.id.projectItem_subtitleTextView)

                projectItemImageView.setImageResource(R.drawable.ic_stat_notify)
                titleProjectItemTextView.text = ""
                subtitleProjectItemTextView.text = ""

                val project = listItem as Project?
                if (project == null) {
                    if (currentPage < Math.ceil((totalProjects / limitPerPage).toDouble()) && currentPage < position / limitPerPage) {
                        currentPage += 1
                        getProjects(currentPage)
                    }
                    return
                }
                titleProjectItemTextView.text = project.name
                subtitleProjectItemTextView.text = project.client
                if (!project.imageSrc.isEmpty()) {
                    ImageCache.getImage(project.imageSrc, context, progressBar) { bitmap: Bitmap?, error: String? ->
                        if (error == null) {
                            projectItemImageView.setImageBitmap(bitmap)
                        }
                    }
                }
                itemView.setOnClickListener { view ->
                    openProject(position, view)
                }
            }
        }

        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_project_list, container, false)

        activity?.actionBar?.title = getString(R.string.menu_projects)
        progressBar = view.findViewById(R.id.projectList_progressBar)
        //progressBar?.hide()

        val spinnerList = ArrayList<String>()
        spinnerList.add("All projects")
        spinnerAdapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, spinnerList)
        spinnerAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner = view.findViewById(R.id.projectList_spinner)
        spinner?.adapter = spinnerAdapter
        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                filterByType(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                Log.i(appName, "Nothing selected")
            }

        }

        recyclerView = view.findViewById(R.id.projectList_recyclerView)
        recyclerView?.adapter = adapter
        recyclerView?.layoutManager = GridLayoutManager(context, 3)
        recyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    (activity as MainActivity).hideBottomNavigation(true)
                } else {
                    (activity as MainActivity).hideBottomNavigation(false)
                }
            }
        })

        swipeRefreshLayout = view.findViewById(R.id.projectList_swipeRefreshLayout)
        swipeRefreshLayout?.setOnRefreshListener { reloadProjects() }

        getProjectTypes()
        getProjects(currentPage)

        return view
    }

    fun scrollToTop() {
        recyclerView!!.smoothScrollToPosition(0)
    }

    private fun getProjects(page: Int) {
        val url = ApiUrl.PROJECTS + "?limit=" + limitPerPage + "&offset=" + limitPerPage * page + "&order_by=released_at&order_desc=1&fields=id,type_id,name,client_name,image_src,description_en,description_es"
        val headers = arrayOf(arrayOf("X-Access-Token", ApiUrl.ACCESS_TOKEN))

        AsyncRequestString("GET", url, context) { _: Int, _: Map<String, List<String>>?, response: String?, error: String?, exception:Exception? ->
            swipeRefreshLayout?.isRefreshing = false
            if (error == null) {
                try {
                    val jsonObject = JSONObject(response)
                    if (jsonObject.has("error")) {
                        val err = jsonObject.optString("error")
                        Log.e(appName, err)
                        Toast.makeText(context, err, Toast.LENGTH_SHORT).show()
                    } else {
                        val jsonArray = jsonObject.getJSONArray("projects")
                        val projects = Project.getLocalArray(context) ?: ArrayList()
                        projects.addAll(Project.arrayFromJSONArray(jsonArray))
                        totalProjects = jsonObject.optInt("projects_count", projects.size)
                        Project.saveLocalArray(projects, context)
                        fillListView()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
                fillListView()
            }
        }
                .setHeaders(headers)
                .setContentLoadingProgressBar(progressBar!!)
                .execute()
    }

    private fun getProjectTypes() {
        val url = ApiUrl.PROJECT_TYPES + "?order_by=name_en&fields=id,name_en,name_es"
        val headers = arrayOf(arrayOf("X-Access-Token", ApiUrl.ACCESS_TOKEN))

        AsyncRequestString("GET", url, context) { _: Int, _: Map<String, List<String>>?, response: String?, error: String?, exception:Exception? ->
            if (error == null) {
                try {
                    val jsonObject = JSONObject(response)
                    if (jsonObject.has("error")) {
                        val err = jsonObject.optString("error")
                        Log.e(appName, err)
                        Toast.makeText(context, err, Toast.LENGTH_SHORT).show()
                    } else {
                        val jsonArray = jsonObject.getJSONArray("types")
                        val types = ProjectTypes.arrayFromJSONArray(jsonArray)
                        ProjectTypes.saveLocalArray(types, context)
                        fillSpinner()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
                fillSpinner()
            }
        }
                .setHeaders(headers)
                .execute()
    }

    private fun fillListView() {
        val projects = Project.getLocalArray(context) ?: ArrayList()
        if (projectTypeId > 0) {
            adapter?.list = projects.filter { projectTypeId == it.typeId }
        } else {
            adapter?.list = projects
            adapter?.size = totalProjects
        }
        adapter?.notifyDataSetChanged()
    }

    private fun fillSpinner() {
        val projectTypes = ProjectTypes.getLocalArray(context) ?: ArrayList()
        for (type in projectTypes) {
            spinnerAdapter?.add(type.localizedName)
        }
        spinnerAdapter?.notifyDataSetChanged()
    }

    private fun filterByType(position: Int) {
        val projectTypes = ProjectTypes.getLocalArray(context) ?: ArrayList()
        if (position < 1) {
            projectTypeId = 0
        } else if (position < projectTypes.size + 1) {
            projectTypeId = projectTypes[position - 1].id
        } else {
            Log.w(appName, "Cannot filter: size ${projectTypes.size} is lower than position $position")
            projectTypeId = 0
        }
        fillListView()
    }

    private fun openProject(position: Int, view: View? = null) {
        val project = adapter?.getItem(position) as Project? ?: return
        val intent = Intent(context, ProjectDetailsActivity::class.java)
        intent.putExtra("project", project)
        if (view != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val imageView = view.findViewById<ImageView>(R.id.projectItem_imageView)
            val pair = android.support.v4.util.Pair.create(imageView as View, imageView.transitionName)
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity as Activity, pair)
            startActivity(intent, options.toBundle())
        } else {
            startActivity(intent)
        }
    }

    private fun reloadProjects() {
        currentPage = 0
        totalProjects = 0
        Project.removeLocalArray(context)
        fillListView()
        getProjects(currentPage)
    }
}

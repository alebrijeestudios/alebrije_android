package com.alebrije.android


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.ContentLoadingProgressBar
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast

import com.alebrije.android.models.News
import com.alebrije.async.AsyncRequestString
import com.alebrije.core.SimpleRecyclerAdapter
import com.google.firebase.messaging.FirebaseMessaging
import org.json.JSONObject

import java.util.ArrayList


/**
 * A simple [Fragment] subclass.
 */
class NewsListFragment : Fragment() {
    private val limitPerPage = 30

    private var adapter: SimpleRecyclerAdapter? = null
    private var appName = ""
    private var progressBar: ContentLoadingProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var sharedPreferences: SharedPreferences? =  null

    private var currentPage = 0
    private var totalNews = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        appName = getString(R.string.app_name)
        sharedPreferences = activity?.getSharedPreferences(appName, Context.MODE_PRIVATE)

        adapter = object : SimpleRecyclerAdapter(R.layout.fragment_project_list_item) {
            override fun onListItem(listItem: Any?, itemView: View, position: Int) {
                val descriptionNewsItemTextView = itemView.findViewById<TextView>(R.id.newsItem_descriptionTextView)

                val news = listItem as News?
                if (news == null) {
                    if (currentPage < Math.ceil((totalNews / limitPerPage).toDouble()) && currentPage < position / limitPerPage) {
                        currentPage += 1
                        getNews(currentPage)
                    }
                    return
                }
                descriptionNewsItemTextView.text = news.localizedDescription
            }
        }

        // Get news subscription
        val subscribedNews: Boolean
        if (sharedPreferences!!.contains("subscribedNews")) {
            subscribedNews = sharedPreferences!!.getBoolean("subscribedNews", false)
        } else {
            subscribedNews = true
            setNewsSubscription(true)
        }

        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_news_list, container, false)

        activity?.actionBar?.title = getString(R.string.menu_news)
        progressBar = view.findViewById(R.id.newsList_progressBar)
        progressBar?.hide()

        val newsSwitch = view.findViewById<Switch>(R.id.newsList_switch)
        newsSwitch.isChecked = subscribedNews
        newsSwitch.setOnCheckedChangeListener { buttonView, isChecked -> setNewsSubscription(isChecked) }

        recyclerView = view.findViewById(R.id.newsList_recyclerView)
        recyclerView?.adapter = adapter
        recyclerView?.layoutManager = GridLayoutManager(context, 1)

        //getNews(currentPage);

        return view
    }

    fun scrollToTop() {
        recyclerView?.smoothScrollToPosition(0)
    }

    private fun setNewsSubscription(on: Boolean) {
        if (on) {
            Log.i(appName, "Subscribed to news")
            FirebaseMessaging.getInstance().subscribeToTopic("news")
        } else {
            Log.i(appName, "Unsubscribed from news")
            FirebaseMessaging.getInstance().unsubscribeFromTopic("news")
        }

        // Save preference
        sharedPreferences?.edit()
                ?.putBoolean("subscribedNews", on)
                ?.apply()
    }

    private fun getNews(page: Int) {
        val url = ApiUrl.NEWS + "?limit=" + limitPerPage + "&offset=" + limitPerPage * page + "&order_by=id&order_desc=1fields=id,description_en,description_es"
        val headers = arrayOf(arrayOf("X-Access-Token", ApiUrl.ACCESS_TOKEN))

        AsyncRequestString("GET", url, context) { _: Int, _: Map<String, List<String>>?, response: String?, error: String?, exception:Exception? ->
            if (error == null) {
                try {
                    val jsonObject = JSONObject(response)
                    if (jsonObject.has("error")) {
                        val err = jsonObject.optString("error")
                        Log.e(appName, err)
                        Toast.makeText(context, err, Toast.LENGTH_SHORT).show()
                    } else {
                        val jsonArray = jsonObject.getJSONArray("news")
                        val news = News.getLocalArray(context) ?: ArrayList()
                        news.addAll(News.arrayFromJSONArray(jsonArray))
                        totalNews = jsonObject.optInt("projects_count", news.size)
                        News.saveLocalArray(news, context)
                        fillListView()
                        fillListView()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
                fillListView()
            }
        }
                .setHeaders(headers)
                .setContentLoadingProgressBar(progressBar!!)
                .execute()
    }

    private fun fillListView() {
        var news = News.getLocalArray(context)
        if (news == null) {
            news = ArrayList()
        }
        adapter?.list = news
        adapter?.size = totalNews

        adapter?.notifyDataSetChanged()
    }
}

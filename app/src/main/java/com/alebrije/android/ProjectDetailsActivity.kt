package com.alebrije.android


import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView

import com.alebrije.android.models.Project
import com.alebrije.async.ImageCache


/**
 * A simple [Fragment] subclass.
 */
class ProjectDetailsActivity : AppCompatActivity() {
    private var appName = ""
    private var projectDetailsImageView: ImageView? = null
    private var project: Project? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_project_details)

        appName = getString(R.string.app_name)

        project = intent.extras.getSerializable("project") as? Project
        if (project == null) {
            finish()
        }

        setSupportActionBar(findViewById<Toolbar>(R.id.projectDetails_toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        findViewById<CollapsingToolbarLayout>(R.id.projectDetails_toolbarLayout).title = project!!.name

        projectDetailsImageView = findViewById(R.id.projectDetails_imageView)
        findViewById<TextView>(R.id.projectDetails_titleTextView).text = project!!.name
        findViewById<TextView>(R.id.projectDetails_subtitleTextView).text = project!!.client
        findViewById<TextView>(R.id.projectDetails_descriptionTextView).text = project!!.localizedDescription
        if (project!!.imageSrc.isEmpty()) {
            projectDetailsImageView?.setImageResource(R.drawable.ic_stat_notify)
        } else {
            ImageCache.getImage(project!!.imageSrc, this) { bitmap: Bitmap?, error: String? ->
                if (error == null) {
                    projectDetailsImageView?.setImageBitmap(bitmap)
                }
            }
        }

        findViewById<View>(R.id.projectDetails_shareButton).setOnClickListener { shareLink() }

        findViewById<View>(R.id.projectDetails_bottomNavigationBarView).visibility = if (hasNavBar()) View.VISIBLE else View.GONE
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun shareLink() {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, project?.name)
        intent.putExtra(Intent.EXTRA_TEXT, "https://alebrije.co")
        startActivity(Intent.createChooser(intent, "Share with"))
    }

    private fun hasNavBar (): Boolean {
        val id = resources.getIdentifier("config_showNavigationBar", "bool", "android")
        return id > 0 && resources.getBoolean(id)
    }
}

package com.alebrije.android


import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.NestedScrollView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.alebrije.android.models.User
import com.alebrije.barcode.AsyncGenerateBarCode
import com.alebrije.core.SimpleFragmentManager
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter


/**
 * A simple [Fragment] subclass.
 */
class UserFragment : Fragment() {
    private var appName = ""
    private var scrollView: NestedScrollView? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        appName = getString(R.string.app_name)

        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_user, container, false)

        val user = User.getLocal(context)
        if (user == null) {
            SimpleFragmentManager.setRootFragment(LoginFragment())
            return view
        }

        activity?.actionBar?.title = getString(R.string.menu_account)
        scrollView = view.findViewById(R.id.user_scrollView)

        view.findViewById<TextView>(R.id.user_nameTextView).text = user.name
        view.findViewById<TextView>(R.id.user_emailTextView).text = user.email
        view.findViewById<View>(R.id.user_logout).setOnClickListener { logout() }

        val qrSize = resources.getDimensionPixelSize(R.dimen.qr_size)
        AsyncGenerateBarCode("User:" + user.id, qrSize, qrSize) { response: Bitmap ->
            view.findViewById<ImageView>(R.id.user_qrImageView).setImageBitmap(response)
        }
                .setContentLoadingProgressBar(view.findViewById(R.id.user_progressBar))
                .setFormat(BarcodeFormat.QR_CODE, QRCodeWriter())
                .execute()

        return view
    }

    fun scrollToTop() {
        scrollView?.smoothScrollTo(0, 0)
    }

    private fun logout() {
        User.removeLocal(context)
        SimpleFragmentManager.setRootFragment(LoginFragment())
    }
}

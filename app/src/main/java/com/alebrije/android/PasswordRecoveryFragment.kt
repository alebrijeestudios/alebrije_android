package com.alebrije.android


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.ContentLoadingProgressBar
import android.support.v4.widget.NestedScrollView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.alebrije.async.AsyncRequestString
import com.alebrije.core.SimpleFragmentManager
import org.json.JSONObject


/**
 * A simple [Fragment] subclass.
 */
class PasswordRecoveryFragment : Fragment() {
    private var appName = ""
    private var progressBar: ContentLoadingProgressBar? = null
    private var scrollView: NestedScrollView? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        appName = getString(R.string.app_name)

        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_password_recovery, container, false)

        activity?.actionBar?.title = getString(R.string.password_recovery)
        progressBar = view.findViewById(R.id.passwordRecovery_progressBar)
        progressBar?.hide()

        scrollView = view.findViewById(R.id.passwordRecovery_scrollView)

        view.findViewById<View>(R.id.passwordRecovery_sendButton).setOnClickListener { recoverPassword(view.findViewById(R.id.passwordRecovery_emailEditText)) }

        return view
    }

    fun scrollToTop() {
        scrollView?.smoothScrollTo(0, 0)
    }

    private fun recoverPassword(emailEditText: EditText) {
        if (emailEditText.text.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(emailEditText.text).matches()) {
            emailEditText.error = getString(R.string.email_error)
            return
        } else {
            emailEditText.error = null
        }

        val url = ApiUrl.PASSWORD_RECOVERY
        val headers = arrayOf(arrayOf("X-Access-Token", ApiUrl.ACCESS_TOKEN))
        val data = arrayOf(arrayOf("email",  emailEditText.text.toString()))

        AsyncRequestString("POST", url, context) { _: Int, _: Map<String, List<String>>?, response: String?, error: String?, exception:Exception? ->
            if (error == null) {
                try {
                    val jsonObject = JSONObject(response)
                    if (jsonObject.has("error")) {
                        val err = jsonObject.optString("error")
                        Log.e(appName, err)
                        Toast.makeText(context, err, Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(context, R.string.password_recovery_success, Toast.LENGTH_SHORT).show()
                        SimpleFragmentManager.popFragment()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
            }
        }
                .setHeaders(headers)
                .setData(data)
                .setContentLoadingProgressBar(progressBar!!)
                .execute()
    }
}

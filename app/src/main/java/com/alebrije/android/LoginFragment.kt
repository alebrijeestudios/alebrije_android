package com.alebrije.android


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.ContentLoadingProgressBar
import android.support.v4.widget.NestedScrollView
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.alebrije.android.models.User
import com.alebrije.async.AsyncRequestString
import com.alebrije.core.SimpleFragmentManager

import com.alebrije.customview.CustomView
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment() {
    private var appName = ""
    private var progressBar: ContentLoadingProgressBar? = null
    private var scrollView: NestedScrollView? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        appName = getString(R.string.app_name)

        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_login, container, false)

        activity?.actionBar?.title = getString(R.string.login)
        progressBar = view.findViewById(R.id.login_progressBar)
        progressBar?.hide()

        scrollView = view.findViewById(R.id.login_scrollView)

        val sendButton = view.findViewById<View>(R.id.login_sendButton)
        sendButton.setOnClickListener { login(view.findViewById(R.id.login_emailEditText), view.findViewById(R.id.login_passwordEditText)) }

        val customView = CustomView(sendButton, CustomView.MODE_PRESSED)
        customView.viewShape.setColor(ContextCompat.getColor(context!!, R.color.colorAccent))
        customView.viewShapePressed.setColor(ContextCompat.getColor(context!!, R.color.colorPrimary))

        view.findViewById<View>(R.id.login_forgotPasswordButton).setOnClickListener { openPasswordRecovery() }

        return view
    }

    fun scrollToTop() {
        scrollView?.smoothScrollTo(0, 0)
    }

    private fun openPasswordRecovery() {
        SimpleFragmentManager.pushFragment(PasswordRecoveryFragment())
    }

    private fun login(emailEditText: EditText, passwordEditText: EditText) {
        if (emailEditText.text.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(emailEditText.text).matches()) {
            emailEditText.error = getString(R.string.email_error)
            return
        } else {
            emailEditText.error = null
        }
        if (passwordEditText.text.isEmpty()) {
            passwordEditText.error = getString(R.string.password_error)
            return
        } else {
            passwordEditText.error = null
        }

        val url = ApiUrl.USERS + "?email=" + emailEditText.text.toString() + "&password=" + md5(passwordEditText.text.toString()) + "&fields=id,name,email"
        val headers = arrayOf(arrayOf("X-Access-Token", ApiUrl.ACCESS_TOKEN))

        AsyncRequestString("GET", url, context) { _: Int, _: Map<String, List<String>>?, response: String?, error: String?, exception: Exception? ->
            if (error == null) {
                try {
                    val jsonObject = JSONObject(response)
                    if (jsonObject.has("error")) {
                        val err = jsonObject.optString("error")
                        Log.e(appName, err)
                        Toast.makeText(context, err, Toast.LENGTH_SHORT).show()
                    } else {
                        val jsonArray = jsonObject.getJSONArray("users")
                        val users = User.arrayFromJSONArray(jsonArray)
                        if (users.size < 1) {
                            val err = getString(R.string.user_not_found)
                            Log.e(appName, err)
                            Toast.makeText(context, err, Toast.LENGTH_SHORT).show()
                        } else {
                            users[0].saveLocal(context)
                            SimpleFragmentManager.setRootFragment(UserFragment())
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
            }
        }
                .setHeaders(headers)
                .setContentLoadingProgressBar(progressBar!!)
                .execute()
    }

    private fun md5(string: String): String {
        try {
            // Create MD5 Hash
            val digest = java.security.MessageDigest.getInstance("MD5")
            digest.update(string.toByteArray())
            val messageDigest = digest.digest()

            // Create Hex String
            val hexString = StringBuilder()
            for (message in messageDigest) {
                var h = Integer.toHexString(0xFF and message.toInt())
                while (h.length < 2)
                    h = "0" + h
                hexString.append(h)
            }
            return hexString.toString()

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return ""
    }
}

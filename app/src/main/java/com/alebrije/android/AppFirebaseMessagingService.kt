package com.alebrije.android

import android.util.Log

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

/**
 *
 * Created by alebrije4 on 11/10/16.
 */

class AppFirebaseMessagingService : FirebaseMessagingService() {
    private var logTag = "AppFMS"

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)

        if (remoteMessage == null) {
            Log.d(logTag, "Message is null")
            return
        }

        Log.d(logTag, "From: " + remoteMessage.from)

        if (remoteMessage.data.isNotEmpty()) {
            Log.d(logTag, "Message data payload: " + remoteMessage.data)
        }
        if (remoteMessage.notification != null) {
            Log.d(logTag, "Message Notification Body: " + remoteMessage.notification?.body)
        }
    }
}

package com.alebrije.customview;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.view.View;

public class CustomView {
    public static int MODE_STATIC = 0;
    public static int MODE_PRESSED = 1;

    private GradientDrawable viewShape;
    private GradientDrawable viewShapePressed;

    /**
     *
     * @param view the view to apply the custom view
     * @param mode the view mode
     */
    public CustomView(View view, int mode) {
        viewShape = new GradientDrawable();
        viewShapePressed = new GradientDrawable();
        StateListDrawable states = new StateListDrawable();

        viewShape.setColor(Color.WHITE);
        viewShape.setCornerRadius(64);
        viewShape.setShape(GradientDrawable.RECTANGLE);

        if (mode == MODE_PRESSED) {
            viewShapePressed.setColor(Color.WHITE);
            viewShapePressed.setCornerRadius(64);
            viewShapePressed.setShape(GradientDrawable.RECTANGLE);

            states.addState(new int[] {android.R.attr.state_pressed}, viewShapePressed);
        }

        states.addState(new int[0], viewShape);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable(states);
        } else {
            view.setBackground(states);
        }
    }

    /**
     *
     * @return the current view shape
     */
    public GradientDrawable getViewShape() {
        return viewShape;
    }

    /**
     *
     * @return the current view shape pressed
     */
    public GradientDrawable getViewShapePressed() {
        return viewShapePressed;
    }
}

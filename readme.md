# Alebrije Libraries for Android

## Configure a project

Inside `.gradle`, add a file `gradle.properties` with your Bitbucket username and password:

```
BITBUCKET_USERNAME your_username
BITBUCKET_PASSWORD your_password
```

In the root `build.gradle`, add the maven repository:

```
allprojects {
    repositories {
        maven {
            credentials {
                username BITBUCKET_USERNAME
                password BITBUCKET_PASSWORD
            }
            url "https://api.bitbucket.org/1.0/repositories/alebrijeestudios/alebrije_android/raw/releases"
        }
    }
}
```

In your app `build.gradle`, add the libraries you require:

```
    implementation 'com.alebrije.libs:core:2.1.4'
    implementation 'com.alebrije.libs:async:2.1.2'
    implementation 'com.alebrije.libs:barcode:2.1.1'
    implementation 'com.alebrije.libs:customview:2.1.1'
    implementation 'com.alebrije.libs:dialogs:2.1.1'
```

## Update the libraries in the repository:

```
./gradlew -p MODULE_NAME assembleRelease uploadArchives -info
```

For this command to work right, you have to add your SSH key before to your Bitbucket account. If you haven't add it already, follow these steps:

1. Generate an SSH key if you don't have any already:

```
ssh-keygen
```

2. Copy your key:

Mac:
```
cat ~/.ssh/id_rsa.pub | pbcopy
```

Windows:
```
cat C:/Users/User/.ssh/id_rsa.pub | clip
```

3. Add a new key in `bitbucket.org > [Avatar] > Bitbucket settings > SSH keys` and paste your key.

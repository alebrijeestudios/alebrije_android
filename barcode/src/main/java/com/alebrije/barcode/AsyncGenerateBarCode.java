package com.alebrije.barcode;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.widget.ContentLoadingProgressBar;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Writer;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;

import java.lang.ref.WeakReference;


public class AsyncGenerateBarCode extends AsyncTask<Void, Integer, Bitmap> {
    private BarcodeFormat format = BarcodeFormat.CODE_128;
    private OnResponseListener listener;
    private String data;
    private WeakReference<ContentLoadingProgressBar> weakProgressBar;
    private Writer writer = new Code128Writer();
    private int background = Color.WHITE;
    private int color = Color.BLACK;
    private int width;
    private int height;


    /**
     *
     * @param data the contents to encode in the barcode
     * @param width the preferred width in pixels
     * @param height the preferred height in pixels
     * @param listener the callback that will run
     */
    public AsyncGenerateBarCode(String data, int width, int height, OnResponseListener listener) {
        this.listener = listener;
        this.data = data;
        this.width = width;
        this.height = height;
    }

    /**
     *
     * @param data the contents to encode in the barcode
     * @param listener the callback that will run
     */
    public AsyncGenerateBarCode(String data, OnResponseListener listener) {
        this(data, 400, 250, listener);
    }

    @Override
    protected void onPreExecute() {
        ContentLoadingProgressBar progressBar = weakProgressBar == null ? null : weakProgressBar.get();
        if (progressBar != null) {
            progressBar.setIndeterminate(false);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        ContentLoadingProgressBar progressBar = weakProgressBar == null ? null : weakProgressBar.get();
        if (progressBar != null) {
            progressBar.setProgress(values[0]);
        }
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        try {
            BitMatrix bm = writer.encode(data, format, width, height);
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    bitmap.setPixel(i, j, bm.get(i, j) ? color : background);
                }
                publishProgress((i * 100) / width);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap response) {
        ContentLoadingProgressBar progressBar = weakProgressBar == null ? null : weakProgressBar.get();
        if (progressBar != null) {
            progressBar.hide();
        }
        if (listener != null) {
            listener.onResponse(response);
        }
    }

    /**
     *
     * @param background the background color for the barcode
     * @return this Async Generator object to allow for chaining of calls to set methods.
     */
    public AsyncGenerateBarCode setBackground(int background) {
        this.background = background;
        return this;
    }

    /**
     *
     * @param color the foreground color for the barcode
     * @return this Async Generator object to allow for chaining of calls to set methods.
     */
    public AsyncGenerateBarCode setColor(int color) {
        this.color = color;
        return this;
    }

    /**
     *
     * @param progressBar the content loading progress bar to update with the request progress
     * @return this Async Generator object to allow for chaining of calls to set methods.
     */
    public AsyncGenerateBarCode setContentLoadingProgressBar(ContentLoadingProgressBar progressBar) {
        this.weakProgressBar = new WeakReference<>(progressBar);
        return this;
    }

    /**
     *
     * @param format the barcode format to generate
     * @param writer the writer that will be used for encoding/generating the barcode image
     * @return this Async Generator object to allow for chaining of calls to set methods.
     */
    public AsyncGenerateBarCode setFormat(BarcodeFormat format, Writer writer) {
        this.format = format;
        this.writer = writer;
        return this;
    }

    public interface OnResponseListener {
        /**
         *
         * @param response the response bitmap
         */
        void onResponse(@NonNull Bitmap response);
    }
}

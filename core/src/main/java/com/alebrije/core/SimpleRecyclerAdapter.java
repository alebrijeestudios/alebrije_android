package com.alebrije.core;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by alebrije4 on 1/20/17.
 *
 */

public abstract class SimpleRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<?> _list;
    private int _size;
    private int _layoutIdView;

    /**
     *
     * @param layoutIdView ID for an XML layout resource to load
     */
    public SimpleRecyclerAdapter(@LayoutRes int layoutIdView) {
        this(layoutIdView, null);
    }

    /**
     *
     * @param layoutIdView ID for an XML layout resource to load
     * @param list the list used for this adapter
     */
    public SimpleRecyclerAdapter(@LayoutRes int layoutIdView, List<?> list) {
        this._layoutIdView = layoutIdView;
        this._list = list;
        this._size = (list == null) ? 0 : list.size();
    }

    @Override
    public int getItemCount() {
        return _size;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(_layoutIdView, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        onListItem((position < _list.size()) ? _list.get(position) : null, holder.itemView, position);
    }

    /**
     *
     * @param position the position for the requested item
     * @return the item at this position
     */
    public Object getItem(int position) {
        return (position < _list.size()) ? _list.get(position) : null;
    }

    /**
     *
     * @return the list for this adapter
     */
    public List<?> getList () {
        return _list;
    }

    /**
     *
     * @return the size for this adapter
     */
    public int getSize() {
        return _size;
    }

    /**
     *
     * @param list the list for this adapter
     */
    public void setList(List<?> list) {
        this._list = list;
        this._size = list.size();
    }

    /**
     *
     * @param size the size for this adapter
     */
    public void setSize(int size) {
        this._size = size;
    }

    /**
     *
     * @param listItem the list item
     * @param view the view for this list item
     * @param position the position of this list item
     */
    public abstract void onListItem(Object listItem, View view, int position);

    private static class ViewHolder extends RecyclerView.ViewHolder {
        private ViewHolder(View itemView) {
            super(itemView);
        }
    }
}

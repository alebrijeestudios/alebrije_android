package com.alebrije.core;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class SimpleFragmentManagerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SimpleFragmentManager._logTag = getString(R.string.app_name);
        SimpleFragmentManager._actionBar = getSupportActionBar();
        SimpleFragmentManager._fragmentManager = getSupportFragmentManager();
    }

    @Override
    protected void onResume() {
        super.onResume();

        SimpleFragmentManager._actionBar = getSupportActionBar();
        SimpleFragmentManager._fragmentManager = getSupportFragmentManager();
    }

    @Override
    public void onBackPressed() {
        if (SimpleFragmentManager._fragmentManager != null && SimpleFragmentManager._fragmentManager.getBackStackEntryCount() > 0) {
            SimpleFragmentManager.popFragment();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }
}

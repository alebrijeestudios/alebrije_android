package com.alebrije.core;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * @deprecated Use SimpleRecyclerAdapter instead when possible.
 */
@Deprecated
public abstract class SimpleListAdapter extends BaseAdapter {
    private List<?> list;
    private int size;
    private int layoutIdView;

    /**
     *
     * @param layoutIdView ID for an XML layout resource to load
     */
    public SimpleListAdapter(@LayoutRes int layoutIdView) {
        this(layoutIdView, null);
    }

    /**
     *
     * @param layoutIdView ID for an XML layout resource to load
     * @param list the list used for this adapter
     */
    public SimpleListAdapter(@LayoutRes int layoutIdView, List<?> list) {
        this.layoutIdView = layoutIdView;
        this.list = list;
        this.size = (list == null) ? 0 : list.size();
    }

    @Override
    public int getCount() {
        return size;
    }

    @Override
    public Object getItem(int position) {
        return position < list.size() ? list.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View reuse, ViewGroup parent) {
        ViewGroup item = getViewGroup(reuse, parent);
        if (position < list.size()) {
            onListItem(list.get(position), item, position);
        } else {
            onListItem(null, item, position);
        }
        return item;
    }

    private ViewGroup getViewGroup(View reuse, ViewGroup parent) {
        if (reuse instanceof ViewGroup) {
            return (ViewGroup) reuse;
        } else {
            Context context = parent.getContext();
            LayoutInflater inflater = LayoutInflater.from(context);
            return (ViewGroup) inflater.inflate(layoutIdView, null);
        }
    }

    /**
     *
     * @return the list for this adapter
     */
    public List<?> getList () {
        return list;
    }

    /**
     *
     * @return the size for this adapter
     */
    public int getSize() {
        return size;
    }

    /**
     * 
     * @param list the list for this adapter
     */
    public void setList(List<?> list) {
        this.list = list;
        this.size = list.size();
    }

    /**
     *
     * @param size the size for this adapter
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     *
     * @param listItem the list item
     * @param view the view for this list item
     * @param position the position of this list item
     */
    public abstract void onListItem(Object listItem, View view, int position);
}
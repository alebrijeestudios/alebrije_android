package com.alebrije.core;

import android.support.annotation.AnimRes;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.util.Log;

public class SimpleFragmentManager {
    protected static ActionBar _actionBar;
    protected static FragmentManager _fragmentManager;
    protected static String _logTag;
    private static int _container = -1;
    private static int _enter = -1;
    private static int _exit = -1;
    private static int _popEnter = -1;
    private static int _popExit = -1;


    /**
     * Pops a fragment from the back stack and updates the default action bar home button.
     */
    public static void popFragment() {
        if (_fragmentManager == null) {
            Log.w(_logTag, "Fragment manager is unavailable.");
            return;
        }
        _fragmentManager.popBackStack();

        if (_actionBar != null && _fragmentManager.getBackStackEntryCount() <= 2) {
            _actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    /**
     * Pushes a new fragment to the back stack and updates the default action bar home button.
     *
     * @param fragment the fragment to be pushed
     */
    public static void pushFragment(Fragment fragment) {
        if (_container == -1) {
            Log.w(_logTag, "Container has not been set.");
            return;
        }
        if (_fragmentManager == null) {
            Log.w(_logTag, "Fragment manager is unavailable.");
            return;
        }
        Fragment currentFragment = _fragmentManager.findFragmentById(_container);
        if (currentFragment == null || !(currentFragment.getClass().isAssignableFrom(fragment.getClass()))) {
            FragmentTransaction transaction = _fragmentManager.beginTransaction();
            if (_enter > -1 && _exit > -1) {
                if (_popEnter > -1 && _popExit > -1) {
                    transaction.setCustomAnimations(_enter, _exit, _popEnter, _popExit);
                } else {
                    transaction.setCustomAnimations(_enter, _exit);
                }
            }
            transaction.replace(_container, fragment);
            transaction.addToBackStack(null);
            transaction.commitAllowingStateLoss();

            if (_actionBar != null) {
                _actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    /**
     * Replaces the current fragment with a new fragment.
     *
     * @param fragment the fragment to be set
     */
    public static void setFragment(Fragment fragment) {
        if (_container == -1) {
            Log.w(_logTag, "Container has not been set.");
            return;
        }
        if (_fragmentManager == null) {
            Log.w(_logTag, "Fragment manager is unavailable.");
            return;
        }
        _fragmentManager.popBackStackImmediate();
        FragmentTransaction transaction = _fragmentManager.beginTransaction();
        if (_enter > -1 && _exit > -1) {
            if (_popEnter > -1 && _popExit > -1) {
                transaction.setCustomAnimations(_enter, _exit, _popEnter, _popExit);
            } else {
                transaction.setCustomAnimations(_enter, _exit);
            }
        }
        if (_fragmentManager.getBackStackEntryCount() > 0) {
            transaction.addToBackStack(null);
            transaction.replace(_container, fragment);
        }else{
            transaction.replace(_container, fragment, "root");
            transaction.addToBackStack("RootFragment");
        }
        transaction.commitAllowingStateLoss();
    }

    /**
     * Removes all back stack and sets a default root fragment and updates the default action bar home button.
     *
     * @param fragment the fragment to be set
     */
    public static void setRootFragment(Fragment fragment) {
        if (_fragmentManager == null) {
            Log.w(_logTag, "Fragment manager is unavailable.");
            return;
        }
        while(_fragmentManager.getBackStackEntryCount() > 0){
            _fragmentManager.popBackStackImmediate();
        }

        if(fragment != null){
            Fragment frag = _fragmentManager.findFragmentByTag("root");
            FragmentTransaction transaction = _fragmentManager.beginTransaction();
            if(_fragmentManager.getBackStackEntryCount() == 0){
                if (_enter > -1 && _exit > -1) {
                    if (_popEnter > -1 && _popExit > -1) {
                        transaction.setCustomAnimations(_enter, _exit, _popEnter, _popExit);
                    } else {
                        transaction.setCustomAnimations(_enter, _exit);
                    }
                }
            }
            if(frag != null) {
                transaction.remove(frag);
            }
            transaction.replace(_container, fragment, "root");
            transaction.addToBackStack("RootFragment");
            transaction.commitAllowingStateLoss();
        }
        //_fragmentManager.popBackStack("RootFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        /*_fragmentManager.popBackStackImmediate(0, 1);
        //_fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (fragment != null) {
            //setFragment(fragment);
            _fragmentManager.beginTransaction().replace(_container, fragment).addToBackStack(null).commit();
        }*/

        if (_actionBar != null) {
            _actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    /**
     * Sets the default container for the fragment manager
     *
     * @param resId the resource id for the fragment container
     */
    public static void setContainer(@IdRes int resId) {
        _container = resId;
    }

    /**
     * Set the default specific animation resources to run on transactions. These animations will not be played when popping the back stack.
     *
     * @param enter int
     * @param exit int
     */
    public static void setCustomAnimations(@AnimRes int enter, @AnimRes int exit) {
        setCustomAnimations(enter, exit, -1, -1);
    }

    /**
     * Set the default specific animation resources to run on transactions. The popEnter and popExit animations will be played for enter/exit operations specifically when popping the back stack.
     *
     * @param enter int
     * @param exit int
     * @param popEnter int
     * @param popExit int
     */
    public static void setCustomAnimations(@AnimRes int enter, @AnimRes int exit, @AnimRes int popEnter, @AnimRes int popExit) {
        _enter = enter;
        _exit = exit;
        _popEnter = popEnter;
        _popExit = popExit;
    }

    /**
     * Clears the default custom animations.
     */
    public static void clearCustomAnimations() {
        _enter = -1;
        _exit = -1;
        _popEnter = -1;
        _popExit = -1;
    }
}

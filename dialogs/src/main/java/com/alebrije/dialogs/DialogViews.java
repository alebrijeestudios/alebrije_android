package com.alebrije.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DialogViews {
    private Context context;
    private Dialog dialog;
    public RelativeLayout mainRelativeLayout;
    public ImageView closeButton;
    public TextView titleTextView;
    public ImageView pictureImageView;
    public TextView infoTextView;
    public Button acceptButton;

    /**
     *
     * @param context
     */
    public DialogViews(Context context) {
        this.context = context;
        this.dialog = new Dialog(this.context);
    }

    /**
     *
     */
    public void customDialog () {
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.sample_dialog);

        mainRelativeLayout = dialog.findViewById(R.id.mainRelativeLayoutDialogAlebrije);
        closeButton = dialog.findViewById(R.id.closeButtonDialogAlebrije);
        titleTextView = dialog.findViewById(R.id.titleTextViewDialogAlebrije);
        pictureImageView = dialog.findViewById(R.id.pictureImageViewDialogAlebrije);
        infoTextView = dialog.findViewById(R.id.infoTextViewDialogAlebrije);
        acceptButton = dialog.findViewById(R.id.acceptButtonDialogAlebrije);

        this.closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        this.acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    /**
     *
     */
    public void acceptDialog () {
        customDialog ();

        mainRelativeLayout.setBackgroundColor(Color.WHITE);

        setTitleTextView("Alert");
        titleTextView.setTextColor(Color.BLUE);
        titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

        setInfoTextView("Message");
        infoTextView.setTextColor(Color.BLACK);
        infoTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);

        setAcceptButton("Accept");
        acceptButton.setTextColor(Color.WHITE);
        acceptButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);

        /*
        RelativeLayout.LayoutParams params =  (RelativeLayout.LayoutParams)  acceptButton.getLayoutParams();
        params.setMargins(64, 20, 64, 20);
        //params.setMarginStart(64);
        //params.setMarginEnd(64);
        acceptButton.setLayoutParams(params);
        */

    }

    /**
     *
     */
    public void dialogShow () {
        try {
            this.dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     */
	public void dialogDismiss () {
        try {
            this.dialog.dismiss();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param closeImage
     */
    public void setCloseButton(Drawable closeImage) {
        this.closeButton.setVisibility(View.VISIBLE);
        this.closeButton.setImageDrawable(closeImage);
    }

    /**
     *
     * @param title
     */
    public void setTitleTextView(String title) {
        this.titleTextView.setVisibility(View.VISIBLE);
        this.titleTextView.setText(title);
    }

    /**
     *
     * @param title
     * @param size
     */
    public void setTitleTextView(String title, int size) {
        this.titleTextView.setVisibility(View.VISIBLE);
        this.titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
        this.titleTextView.setText(title);
    }

    /**
     *
     * @param pictureImage
     */
    public void setPictureImageView(Drawable pictureImage) {
        this.pictureImageView.setVisibility(View.VISIBLE);
        this.pictureImageView.setImageDrawable(pictureImage);
    }

    /**
     *
     * @param pictureImage
     * @param width
     * @param height
     */
	public void setPictureImageView(Drawable pictureImage, int width, int height) {
        final float scale = context.getResources().getDisplayMetrics().density;
        int w = (int) (width * scale + 0.5f);
        int h = (int) (height * scale + 0.5f);

        this.pictureImageView.setVisibility(View.VISIBLE);
        this.pictureImageView.getLayoutParams().width = w;
        this.pictureImageView.getLayoutParams().height = h;
        this.pictureImageView.setImageDrawable(pictureImage);
    }

    /**
     *
     * @param info
     */
    public void setInfoTextView(String info) {
        this.infoTextView.setVisibility(View.VISIBLE);
        this.infoTextView.setText(info);
    }

    /**
     *
     * @param info
     * @param size
     */
    public void setInfoTextView(String info, int size) {
        this.infoTextView.setVisibility(View.VISIBLE);
        this.infoTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
        this.infoTextView.setText(info);
    }

    /**
     *
     * @param name
     */
    public void setAcceptButton(String name) {
        this.acceptButton.setVisibility(View.VISIBLE);
        this.acceptButton.setText(name);
    }

    /**
     *
     * @param name
     * @param size
     */
    public void setAcceptButton(String name, int size) {
        this.acceptButton.setVisibility(View.VISIBLE);
        this.acceptButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
        this.acceptButton.setText(name);
    }

    /**
     *
     */
    public void emptyDialog () {
        RelativeLayout relativeLayout = new RelativeLayout(this.context);
        RelativeLayout.LayoutParams relativeLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        relativeLayout.setPadding(10, 10, 10, 10);

        TextView textView = new TextView(context);
        textView.setText("Test");

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        textView.setLayoutParams(layoutParams);
        relativeLayout.addView(textView);

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(relativeLayout, relativeLayoutParams);

        dialog.show();
    }

}